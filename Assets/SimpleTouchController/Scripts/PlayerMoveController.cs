﻿using UnityEngine;
using System.Collections;



public class PlayerMoveController : MonoBehaviour
{

    // PUBLIC

    public SimpleTouchController leftController;
    public SimpleTouchController rightController;

    public float MoveSpeed = 5f;//брать от дино
    public float RotationSpeed = 3000f;

    // PRIVATE
    private Rigidbody DinoRigidbody;
    private Transform DinoTransform;

    public Follower Camera;

    private float PrevZ = 0.1f;
    private float PrevX = 0.1f;


    void Start()
    {

        
        rightController.TouchEvent += RightController_TouchEvent;
    }
    public void FindPlayer(){
        var MonoDino = GetComponentInChildren<MonoDino>();
        DinoRigidbody = MonoDino.gameObject.GetComponent<Rigidbody>();
        DinoTransform = MonoDino.gameObject.GetComponent<Transform>();

        Camera.Target=DinoTransform;
    }



    void RightController_TouchEvent(Vector2 value)
    {
        UpdateAim(value);
    }

    void FixedUpdate()
    {
        // move
        if (DinoRigidbody != null)
        {
            //DinoRigidbody.MovePosition(DinoTransform.position + (DinoTransform.up * leftController.GetTouchPosition.y * Time.deltaTime * MoveSpeed) +
              //  (DinoTransform.right * leftController.GetTouchPosition.x * Time.deltaTime * MoveSpeed));
            DinoTransform.transform.position=Vector3.MoveTowards(DinoTransform.transform.position,
                             DinoTransform.transform.position + (new Vector3(0,1,0) * leftController.GetTouchPosition.y * MoveSpeed) +
                            (new Vector3(1,0,0) * leftController.GetTouchPosition.x  * MoveSpeed) ,
                            MoveSpeed*Time.deltaTime);
                            
            //print(transform.position.x+" x   "+transform.position.y+" y      "+transform.position.z+" z");
        }
    }

    void UpdateAim(Vector2 value){
        float x1=1;
        float y1=0;
        float x2=value.x;
        float y2=value.y;

        if(x2==0 && y2==0){
            x2=0.01f;
            y2=0.01f;
        }

        float cos=(x1*x2+y1*y2)/(
            Mathf.Sqrt(Mathf.Pow(x1,2)+(Mathf.Pow(y1,2)))*(Mathf.Sqrt(Mathf.Pow(x2,2)+(Mathf.Pow(y2,2))))
            );

        float angle=Mathf.Acos(cos)*180/Mathf.PI;
        //print(x2+" x"+y2+ " y");
        if(y2<0){
            angle=360-angle;
        }
        angle-=90;
//        print(cos+" cos");
        //print(angle);
         DinoTransform.transform.rotation=Quaternion.Euler(DinoTransform.transform.rotation.x, DinoTransform.transform.rotation.y,angle);


    }

    void OnDestroy()
    {
        rightController.TouchEvent -= RightController_TouchEvent;
    }

}
