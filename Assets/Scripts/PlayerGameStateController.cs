﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGameStateController : MonoBehaviour
{
    public static PlayerGameStateController instance { get; private set; }
    public PlayerGameState playerGameState { get; private set; }

    private PlayerGameStateController() { }


    public void setState(PlayerGameState playerGameState)
    {
        this.playerGameState = playerGameState;
    }

    public void setPlaying()
    {
        setState(PlayerGameState.PLAYING);
    }


    void Start()
    {
        instance = this;
        playerGameState = PlayerGameState.CHOOSING_TYPE;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(playerGameState);
    }
}

