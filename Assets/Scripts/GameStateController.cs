﻿using System.Collections;
using System.Collections.Generic;
using dinio_client;
using Newtonsoft.Json;
using UnityEngine;

public class GameStateController : MonoBehaviour
{
    public MonoConnection Connect;
    public static GameStateController Instance { get; private set; }
    public List<GameObject> DinoGOs { get; private set; }

    public List<GameObject> FoodGOs { get; private set; }

    public GameObject PlayerDinoGO { get; set; }
    public GameState GameState;

    public string PlayerName { get; set; }

    public FoodSpawner FoodSpawner;

    public DinoSpawner DinoSpawner;
    public DeathButtons DeathButtons;
    public void AddDino(GameObject dinogo)
    {
        DinoGOs.Add(dinogo);
        GameState.dinos.Add(dinogo.GetComponent<MonoDino>().Dino);


    }


    public void GOToEnt()
    {

        foreach (var dinogo in DinoGOs)
        {
            Dino dino = dinogo.GetComponent<MonoDino>().Dino;
            dino.myTransform.x = dinogo.transform.position.x;
            dino.myTransform.y = dinogo.transform.position.y;
            dino.myTransform.rotation = dinogo.transform.eulerAngles.z;
            dino.myTransform.scaleX = dinogo.transform.localScale.x;
            dino.myTransform.scaleY = dinogo.transform.localScale.y;

            foreach (var elgo in dino.BodyElementGOs)
            {
                BodyElement el = elgo.GetComponent<MonoBodyElement>().BodyElement;
                el.myBoxCollider2D.myTransform.x = elgo.transform.localPosition.x;
                el.myBoxCollider2D.myTransform.y = elgo.transform.localPosition.y;
                // el.myBoxCollider2D.myTransform.rotation = elgo.transform.eulerAngles.z;
                el.myBoxCollider2D.myTransform.scaleX = elgo.transform.localScale.x;
                el.myBoxCollider2D.myTransform.scaleY = elgo.transform.localScale.y;


            }
        }
        //+match food
        foreach (var foodGo in FoodGOs)
        {
            Food food = foodGo.GetComponent<MonoFood>().Food;
            food.myTransform.x = foodGo.transform.position.x;
            food.myTransform.y = foodGo.transform.position.y;
            food.myTransform.rotation = foodGo.transform.eulerAngles.z;
            food.myTransform.scaleX = foodGo.transform.localScale.x;
            food.myTransform.scaleY = foodGo.transform.localScale.y;
        }



    }

    private void PlayerDinoUpdate(Dino dino)
    {
        //not changing player pos
    }
    public void EntToGO()
    {
        foreach (var dinogo in DinoGOs)
        {
            Dino dino = dinogo.GetComponent<MonoDino>().Dino;
            if (PlayerDinoGO != null)
                if (dino.name.Equals(PlayerDinoGO.GetComponent<MonoDino>().Dino.name))//this is player`s dino
                {
                    PlayerDinoUpdate(dino);
                    continue;
                }

            dinogo.transform.position = new Vector3(dino.myTransform.x,
                                    dino.myTransform.y, dinogo.transform.position.z);//local pos earlier

            dinogo.transform.eulerAngles = new Vector3(dinogo.transform.eulerAngles.x,//trans.Angle earlier
                                    dinogo.transform.eulerAngles.y, dino.myTransform.rotation);
            dinogo.transform.localScale = new Vector3(dino.myTransform.scaleX,
                                    dino.myTransform.scaleY, dinogo.transform.localScale.z);


            foreach (var elgo in dino.BodyElementGOs)
            {
                BodyElement el = elgo.GetComponent<MonoBodyElement>().BodyElement;
                elgo.transform.localPosition = new Vector3(el.myBoxCollider2D.myTransform.x,
                                    el.myBoxCollider2D.myTransform.y, elgo.transform.localPosition.z);

                // elgo.transform.eulerAngles = new Vector3(elgo.transform.eulerAngles.x,
                //                        elgo.transform.eulerAngles.y, el.myBoxCollider2D.myTransform.rotation);
                elgo.transform.localScale = new Vector3(el.myBoxCollider2D.myTransform.scaleX,
                                        el.myBoxCollider2D.myTransform.scaleY, elgo.transform.localScale.z);


            }
        }
        foreach (var foodGo in FoodGOs)
        {
            Food food = foodGo.GetComponent<MonoFood>().Food;
            foodGo.transform.position = new Vector3(food.myTransform.x,
                                    food.myTransform.y, foodGo.transform.position.z);//local pos earlier

            foodGo.transform.eulerAngles = new Vector3(foodGo.transform.eulerAngles.x,//trans.Angle earlier
                                    foodGo.transform.eulerAngles.y, food.myTransform.rotation);
            foodGo.transform.localScale = new Vector3(food.myTransform.scaleX,
                                    food.myTransform.scaleY, foodGo.transform.localScale.z);
        }
        //+match food


    }

    private Food FindFoodById(List<Food> list, Food f)
    {
        foreach (var food in list)
        {
            if (food.id == f.id)
                return food;
        }
        return null;
    }
    private Food FindFoodById(List<Food> list, int id)
    {
        foreach (var food in list)
        {
            if (food.id == id)
                return food;
        }
        return null;
    }
    private Dino FindByName(List<Dino> list, Dino d)
    {
        foreach (var dino in list)
        {
            if (dino.name.Equals(d.name))
                return dino;
        }
        return null;
    }
    private Dino FindByName(List<Dino> list, string name)
    {
        foreach (var dino in list)
        {
            if (dino.name.Equals(name))
                return dino;
        }
        return null;
    }

    private GameObject FindGOByDino(Dino d)
    {
        foreach (var dino in DinoGOs)
        {
            if (dino.GetComponent<MonoDino>().Dino.Equals(d))
                return dino;
        }
        return null;
    }
    private GameObject FindGOByFood(Food f)
    {
        foreach (var food in FoodGOs)
        {
            if (food.GetComponent<MonoFood>().Food.Equals(f))
                return food;
        }
        return null;
    }



    public void UpdateState(string state)
    {
        GameState newState = (GameState)JsonMaker.FromJson(state);


        List<Dino> listToDel = new List<Dino>(GameState.dinos);
        List<Dino> listToStay = new List<Dino>();
        List<Dino> listToInst = new List<Dino>();//list for creating new dinos on client by getting extra ones from serv
        for (int i = 0; i < newState.dinos.Count; i++)
        {
            Dino d = newState.dinos[i];
            Dino old = FindByName(listToDel, d);
            if (d.hp > 0)
                if (old != null)
                {
                    listToStay.Add(old);
                    listToDel.Remove(old);
                }
                else
                {
                    listToInst.Add(d);
                }
        }

        //dummy stay code
        var dummy = FindByName(listToDel, "dummy");
        listToDel.Remove(dummy);
        //dummy stay code end

        Dino newstatePlayerDino = FindByName(newState.dinos, PlayerName);
        if (PlayerDinoGO != null &&
            newstatePlayerDino == null)//игрок есть , в новом состоянии нет дино игрока
        {

            Dino playerDino = PlayerDinoGO.GetComponent<MonoDino>().Dino;
            listToDel.Remove(playerDino);
        }
        if (PlayerDinoGO == null &&
            newstatePlayerDino != null)//игрока нет, в новом состоянии есть дино игрока
        {

            //listToDel.Add(playerDino); ?
            listToInst.Remove(newstatePlayerDino);
        }

        for (int i = 0; i < listToDel.Count; i++)
        {
            print("del " + i);
            Dino dinoToDestroy = listToDel[i];
            if (dinoToDestroy.name.Equals(PlayerName))
            {
                DeathButtons.gameObject.SetActive(true);
            }
            GameState.dinos.Remove(dinoToDestroy);
            GameObject dgo = FindGOByDino(dinoToDestroy);
            DinoGOs.Remove(dgo);
            Destroy(dgo);
        }
        for (int i = 0; i < listToInst.Count; i++)
        {
            /*var newFoodGO = DinoSpawner.SpawnDino(listToInst[i]);
            DinoGOs.Add(newFoodGO);
            GameState.dinos.Add(listToInst[i]);*/
        }



        for (int i = 0; i < listToStay.Count; i++)//3newstate -> 3state
        {
            Dino d = listToStay[i];//дино из старого геймстата который должен остаться
            Dino newstateDino = FindByName(newState.dinos, d);//новое сост старого дино
            List<GameObject> bodyElementGOs = d.BodyElementGOs;// сохраняем 1
            d = newstateDino;
            d.BodyElementGOs = bodyElementGOs;
            GameState.SetDinoByName(d);

        }

        /////////////////////////////////////////////////////food 

        List<Food> flistToDel = new List<Food>(GameState.foodList);
        List<Food> flistToStay = new List<Food>();
        List<Food> flistToInst = new List<Food>();//list for creating new dinos on client by getting extra ones from serv
        for (int i = 0; i < newState.foodList.Count; i++)
        {
            Food f = newState.foodList[i];

            Food old = FindFoodById(flistToDel, f);
            if (old != null)
            {

                flistToStay.Add(old);
                flistToDel.Remove(old);
            }
            else
            {

                flistToInst.Add(f);
            }
        }




        for (int i = 0; i < flistToDel.Count; i++)
        {
            // print("del f " + i);
            Food foodToDestroy = flistToDel[i];
            GameState.foodList.Remove(foodToDestroy);
            GameObject fgo = FindGOByFood(foodToDestroy);
            FoodGOs.Remove(fgo);
            Destroy(fgo);
        }




        for (int i = 0; i < flistToStay.Count; i++)//3newstate -> 3state
        {
            Food f = flistToStay[i];//food из старого геймстата который должен остаться
            Food newstateFood = FindFoodById(newState.foodList, f);//новое сост старого food
            f = newstateFood;
            GameState.SetFoodbyId(f);

        }
        for (int i = 0; i < flistToInst.Count; i++)
        {

            var newFoodGO = FoodSpawner.SpawnFood(flistToInst[i]);
            FoodGOs.Add(newFoodGO);
            GameState.foodList.Add(flistToInst[i]);

        }





        //3state -> 3o
        StatesToObjects();
        //3->1
        EntToGO();




    }

    public void ObjectsToStates()
    {
        for (int i = 0; i < DinoGOs.Count; i++)
        {
            GameState.dinos[i] = DinoGOs[i].GetComponent<MonoDino>().Dino;
        }
        for (int i = 0; i < FoodGOs.Count; i++)
        {
            GameState.foodList[i] = FoodGOs[i].GetComponent<MonoFood>().Food;
        }
    }

    public void StatesToObjects()
    {
        for (int i = 0; i < DinoGOs.Count; i++)
        {
            List<GameObject> bodyElementGOs1 = DinoGOs[i].GetComponent<MonoDino>().Dino.BodyElementGOs;// сохраняем 1
            DinoGOs[i].GetComponent<MonoDino>().Dino = GameState.dinos[i];
            DinoGOs[i].GetComponent<MonoDino>().Dino.BodyElementGOs = bodyElementGOs1;
        }
        for (int i = 0; i < FoodGOs.Count; i++)
        {
            FoodGOs[i].GetComponent<MonoFood>().Food = GameState.foodList[i];
        }

    }






    private int i = 0;
    private void FixedUpdate()
    {       /*get
                create 3 
                3 - 3
                update  3- 1 obj

                change

                update 3
                sting send
                */
        i++;
        if (!MsgQueue.instance.isEmpty())
        {
            {
                /* testing data
                 MyTransform myTransform = new MyTransform(1, 2, 3, 4, 5);
                 MyBoxCollider2D boxCollider2D = new MyBoxCollider2D(myTransform);
                 Dino dino = new Dino();
                 dino.myTransform = myTransform;
                 dino.bodyElements = new List<BodyElement>();*/
            }
            //1->3
            GOToEnt();
            //3o -> 3state
            ObjectsToStates();


            //  print(Room.name+" roomname");



            // Debug.Log(MsgQueue.instance.Count());
            // print(GameStateChangeController.instance.GetChanges());

            string s = JsonMaker.ToJson(GameState);
            //print(Room.name + " roomname");
            //Debug.Log("pre convert send");
            //Debug.Log(s);

            Connect.TestConnect.Send(s);

            GameStateChangeController.instance.SetChangesFalse();

            string state = MsgQueue.instance.pop();
            if (!state.Equals(""))
            {


                //Debug.Log("pre upd");
                //Debug.Log(state);

                GameStateController.Instance.UpdateState(state);
            }



            {/*else/*
            testing data
                MyTransform myTransform = new MyTransform(1, 2, 3, 4, 5);
                MyBoxCollider2D boxCollider2D = new MyBoxCollider2D(myTransform);

                BodyElement b = new BodyElement();
                b.myBoxCollider2D = boxCollider2D;

                string json = JsonMaker.ToJson(b);
                print("1sending " + json);
                
                ////////////////////
                Dino dino = new HerbivoreDino();

                dino.bodyElements = new List<BodyElement>();
                var bodyel = new BodyElement();
                bodyel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 0, 0, 10, 20));


                var headel = new ConsumeBodyElement();
                headel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 13, 0, 6, 6));

                dino.bodyElements.Add(bodyel);
                dino.bodyElements.Add(headel);
                //x y where to spawn can be given as params
                dino.myTransform = new MyTransform(0, 0, 0, 1, 1);
                dino.level = new Level();
                dino.hunger = dino.maxHunger = 100;
                dino.hp = dino.maxHp = 100;
                dino.speed = 2;
                dino.rotationSpeed = 2;

                string json = JsonMaker.ToJson(dino);
                print("1sending " + json);


                Connect.TestConnect.Send(json);
            */
            }


        }
        //return;




    }




    private GameStateController() { }

    private void Awake()
    {
        Instance = this;
        DinoGOs = new List<GameObject>();
        FoodGOs = new List<GameObject>();
        GameState = new GameState();
    }
    // Update is called once per frame
    void Update()
    {

    }
}


