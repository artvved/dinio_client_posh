﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateChangeController : MonoBehaviour
{
    public static GameStateChangeController instance { get; private set; }

    private bool hasChanges;
    void Start()
    {
        instance = this;
        hasChanges = false;
    }
    public bool GetChanges(){
        return hasChanges;
    }
    public void SetChangesTrue()
    {
        hasChanges = true;
    }
    public void SetChangesFalse()
    {
        hasChanges = false;
    }
}
