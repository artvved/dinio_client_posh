﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerStatsUIController : MonoBehaviour
{
    public Image HpBar;
    public Image HungerBar;

    public Image ExpBar;

    public Text Level;
    public Text Score;

    public Text Leaderboard;
    
    
    private GameObject DinoGO;
    void Start()
    {
        
    }

    public void FindDino(){
        DinoGO=GameStateController.Instance.PlayerDinoGO;
    }

    
    void Update()
    {
        if(DinoGO!=null){
            
            Dino dino=GameStateController.Instance.PlayerDinoGO.GetComponent<MonoDino>().Dino;
            
            float hp=dino.hp;
            float maxHp=dino.maxHp;
            HpBar.fillAmount=hp/maxHp;

            float hunger=dino.hunger;
            float maxHunger=dino.maxHunger;
            HungerBar.fillAmount=hunger/maxHunger;

            float exp=dino.level.currentExp;
            float maxExp=dino.level.currentExpToNewLevel;
            ExpBar.fillAmount=exp/maxExp;

            Level.text="Уровень : "+ dino.level.currentLevel;

            Score.text="Очки : "+dino.score;
            var dinogos=GameStateController.Instance.DinoGOs;
           // Leaderboard.text=
        }
    }
}
