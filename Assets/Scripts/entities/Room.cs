﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using dinio_client;
using System;

public static class Room
{
    public static string name { get; set; }
    public static int maxCountOfPlayers { get; set; }
    public static int currentCountOfPlayers { get; set; }
    public static bool isPrivate { get; set; }

}
