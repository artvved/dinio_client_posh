﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MyTransform 
{
    public MyTransform(float v1, float v2, float v3, float v4, float v5)
    {
        x = v1;
        y = v2;
        rotation = v3;
        scaleX = v4;
        scaleY = v5;
    }
    public MyTransform(){}
    
    public float x ;
    public float y ;
    public float rotation;
    public float scaleX ;
    public float scaleY ;

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
