﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBodyElement : BodyElement
{

    private int damage;
    public int GetDamage() {
        return damage;
    }

    public void SetDamage(int damage) {
        this.damage = damage;
    }

    public AttackBodyElement()
    {
    }

    public AttackBodyElement(MyBoxCollider2D myBoxCollider2D) : base(myBoxCollider2D)
    {
    }

    
}
