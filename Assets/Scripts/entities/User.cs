﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using dinio_client;
using System;

[Serializable]
public static class User
{
    public static string name { get; set; }
    public static string device_id { get; set; }
}
