﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Level
{
    public int currentLevel;
    public int currentExp;
    public int currentExpToNewLevel;

    public int[] expToNewLevel;

    public Level()
    {
        expToNewLevel = new int[10];
        for (int i = 0; i < expToNewLevel.Length; i++)
        {
            expToNewLevel[i] = 5 * (i);
        }
        currentLevel = 0;
        currentExp = 0;
        currentExpToNewLevel = expToNewLevel[1];
    }

    public void recalculateExp(int exp)
    {
        if (currentLevel == 10)
            return;

        currentExp += exp;
        while (currentExp >= currentExpToNewLevel)
        {
            currentExp -= currentExpToNewLevel;
            currentLevel++;
            currentExpToNewLevel = expToNewLevel[currentLevel + 1];
        }
    }

}
