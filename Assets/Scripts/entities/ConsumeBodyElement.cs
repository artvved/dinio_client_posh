﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumeBodyElement : BodyElement
{
    public ConsumeBodyElement()
    {
    }

    public ConsumeBodyElement(MyBoxCollider2D myBoxCollider2D) : base(myBoxCollider2D)
    {
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return "ConsumeBodyElement{" +
                "boxCollider2D=" + myBoxCollider2D +
                '}';;
    }
}
