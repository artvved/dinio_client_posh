﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Dino
{



    [NonSerialized]
    public List<GameObject> BodyElementGOs;
    
    public List<BodyElement> bodyElements;
    public MyTransform myTransform ;
    public Level level;
    public float hunger;
    public float maxHunger;
    public float hp;
    public float maxHp;
    public double speed;
    public double rotationSpeed;

    public string name;
    public int score;



   
}
