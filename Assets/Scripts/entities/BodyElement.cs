﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class BodyElement 
{
    public BodyElement()
    {
    }

    public BodyElement(MyBoxCollider2D myBoxCollider2D)
    {
        this.myBoxCollider2D = myBoxCollider2D;
    }

    public MyBoxCollider2D myBoxCollider2D;
   


}
