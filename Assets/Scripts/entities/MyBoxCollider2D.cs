﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MyBoxCollider2D 
{
    public MyBoxCollider2D(MyTransform myTransform)
    {
        this.myTransform = myTransform;
    }

    public MyBoxCollider2D()
    {
    }

    public MyTransform myTransform;

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
