﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoSpawner : MonoBehaviour
{
    public GameObject Player;
    public GameObject dinoPrefab;
    public GameObject bodyelPrefab;

    public Material HerbivoreMaterial;
    public Material PredatorMaterial;


    private void Start()
    {
        PlayerPrefs.SetString("name","name");//changable
        Player = GameObject.Find("Player");
        CreateDummy();
    }
    public void CreateDummy()
    {
        GameObject dinogo = Instantiate(dinoPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
        dinogo.transform.SetParent(GameObject.Find("Dinos").transform);
        dinogo.GetComponent<MonoDino>().Dino = new HerbivoreDino();
        Dino dino = dinogo.GetComponent<MonoDino>().Dino;
        dino.BodyElementGOs = new List<GameObject>();
        dino.bodyElements = new List<BodyElement>();

        GameObject bodygo = Instantiate(bodyelPrefab, dinogo.transform);
        BodyElement bodyel = bodygo.GetComponent<MonoBodyElement>().BodyElement;
        bodyel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 0, 0, 10, 20));

        GameObject headgo = Instantiate(bodyelPrefab, dinogo.transform);
        var headel = new ConsumeBodyElement();
        headel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 13, 0, 6, 6));
        headgo.GetComponent<MonoBodyElement>().BodyElement = headel;

        GameObject tailgo = Instantiate(bodyelPrefab, dinogo.transform);
        var tailel = new AttackBodyElement();
        tailel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, -16, 0, 3, 12));
        tailgo.GetComponent<MonoBodyElement>().BodyElement = tailel;


        dino.BodyElementGOs.Add(bodygo);
        dino.bodyElements.Add(bodyel);
        dino.BodyElementGOs.Add(headgo);
        dino.bodyElements.Add(headel);
        dino.BodyElementGOs.Add(tailgo);
        dino.bodyElements.Add(tailel);
        //x y where to spawn can be given as params
        dino.myTransform = new MyTransform(0, 0, 0, 1, 1);
        dino.level = new Level();
        dino.hunger = dino.maxHunger = 100;
        dino.hp = dino.maxHp = 100;
        dino.speed = 2;
        dino.rotationSpeed = 2;
        dino.name = "dummy";
        Match(dinogo);
        SetMaterialToDino(dinogo,HerbivoreMaterial);
        dinogo.transform.position=new Vector3(5,0,0);
    }

    public GameObject SpawnDino(Dino d){
        return new GameObject();
    }

    public void CreatePredatorDino()
    {

        GameObject dinogo = Instantiate(dinoPrefab, new Vector3(0f, 0f, 0f), new Quaternion());
        dinogo.transform.SetParent(Player.transform);
        //dinogo.AddComponent(typeof(Dino));
        dinogo.GetComponent<MonoDino>().Dino = new PredatorDino();
        Dino dino = dinogo.GetComponent<MonoDino>().Dino;
        dino.BodyElementGOs = new List<GameObject>();
        dino.bodyElements = new List<BodyElement>();

        GameObject bodygo = Instantiate(bodyelPrefab, dinogo.transform);
        BodyElement bodyel = bodygo.GetComponent<MonoBodyElement>().BodyElement;
        bodyel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 0, 0, 8, 20));

        GameObject headgo = Instantiate(bodyelPrefab, dinogo.transform);
        var headel = new AttackBodyElement();
        headel.SetDamage(5);
        headel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 12, 0, 12, 12));
        headgo.GetComponent<MonoBodyElement>().BodyElement = headel;

        GameObject tailgo = Instantiate(bodyelPrefab, dinogo.transform);
        var tailel = new BodyElement();
        tailel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, -15, 0, 3, 10));
        tailgo.GetComponent<MonoBodyElement>().BodyElement = tailel;


        dino.BodyElementGOs.Add(bodygo);
        dino.bodyElements.Add(bodyel);
        dino.BodyElementGOs.Add(headgo);
        dino.bodyElements.Add(headel);
        dino.BodyElementGOs.Add(tailgo);
        dino.bodyElements.Add(tailel);
        //x y where to spawn can be given as params
        dino.myTransform = new MyTransform(0, 0, 0, 1, 1);
        dino.level = new Level();
        dino.hunger = dino.maxHunger = 100;
        dino.hp = dino.maxHp = 100;
        dino.speed = 2;
        dino.rotationSpeed = 2;
        dino.name = PlayerPrefs.GetString("name");
        Match(dinogo);
        AddDinoToPlayer(dinogo,dino);

        SetMaterialToDino(dinogo,PredatorMaterial);
    }

    private void SetMaterialToDino(GameObject dinogo,Material material){
        var children=dinogo.transform.GetChildCount();
        for (int i = 0; i < children; i++)
        {
            var child=dinogo.transform.GetChild(i);
            child.gameObject.GetComponent<MeshRenderer>().material=material;
        }
    }


    public void CreateHerbivoreDino()
    {

        GameObject dinogo = Instantiate(dinoPrefab, new Vector3(0f, 0f, 0f), new Quaternion());
        dinogo.transform.SetParent(Player.transform);
        //dinogo.AddComponent(typeof(Dino));
        dinogo.GetComponent<MonoDino>().Dino = new HerbivoreDino();
        Dino dino = dinogo.GetComponent<MonoDino>().Dino;
        dino.BodyElementGOs = new List<GameObject>();
        dino.bodyElements = new List<BodyElement>();

        GameObject bodygo = Instantiate(bodyelPrefab, dinogo.transform);
        BodyElement bodyel = bodygo.GetComponent<MonoBodyElement>().BodyElement;
        bodyel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 0, 0, 10, 20));

        GameObject headgo = Instantiate(bodyelPrefab, dinogo.transform);
        var headel = new ConsumeBodyElement();
        headel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, 13, 0, 6, 6));
        headgo.GetComponent<MonoBodyElement>().BodyElement = headel;

        GameObject tailgo = Instantiate(bodyelPrefab, dinogo.transform);
        var tailel = new AttackBodyElement();
        tailel.SetDamage(5);
        tailel.myBoxCollider2D = new MyBoxCollider2D(new MyTransform(0, -16, 0, 3, 12));
        tailgo.GetComponent<MonoBodyElement>().BodyElement = tailel;


        dino.BodyElementGOs.Add(bodygo);
        dino.bodyElements.Add(bodyel);
        dino.BodyElementGOs.Add(headgo);
        dino.bodyElements.Add(headel);
        dino.BodyElementGOs.Add(tailgo);
        dino.bodyElements.Add(tailel);
        //x y where to spawn can be given as params
        dino.myTransform = new MyTransform(0, 0, 0, 1, 1);
        dino.level = new Level();
        dino.hunger = dino.maxHunger = 100;
        dino.hp = dino.maxHp = 100;
        dino.speed = 2;
        dino.rotationSpeed = 2;
        dino.name = "namepass";
        Match(dinogo);
        AddDinoToPlayer(dinogo,dino);

        SetMaterialToDino(dinogo,HerbivoreMaterial);
    }


    private void Match(GameObject dinogo)
    {

        Dino dino = dinogo.GetComponent<MonoDino>().Dino;


        foreach (var elgo in dino.BodyElementGOs)
        {
            // Debug.Log(elgo.GetComponent<MonoBodyElement>() == null);
            BodyElement el = elgo.GetComponent<MonoBodyElement>().BodyElement;


            elgo.transform.localPosition =
                        new Vector3(el.myBoxCollider2D.myTransform.x,
                                el.myBoxCollider2D.myTransform.y,
                                elgo.transform.localPosition.z);
            elgo.transform.eulerAngles =
                        new Vector3(
                                elgo.transform.localRotation.x,
                                elgo.transform.localRotation.y,
                                el.myBoxCollider2D.myTransform.rotation);

            elgo.transform.localScale =
                        new Vector3(el.myBoxCollider2D.myTransform.scaleX,
                                el.myBoxCollider2D.myTransform.scaleY,
                                elgo.transform.localScale.z);

        }
        dinogo.transform.localPosition =
                            new Vector3(dino.myTransform.x,
                                        dino.myTransform.y,
                                        dinogo.transform.localPosition.z);
        dinogo.transform.eulerAngles =
                        new Vector3(
                                dinogo.transform.localRotation.x,
                                dinogo.transform.localRotation.y,
                                dino.myTransform.rotation);


        GameStateController.Instance.AddDino(dinogo);
       

    }

    private void AddDinoToPlayer(GameObject dinogo,Dino dino){
        GameStateController.Instance.PlayerDinoGO = dinogo;
        GameStateController.Instance.PlayerName = dino.name;

    }



}





