﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{

    public List<Dino> dinos;
    public List<Food> foodList;

    public GameState()
    {
        dinos = new List<Dino>();
        foodList = new List<Food>();
    }

    public void SetDinoByName(Dino d){
        for (int i = 0; i < dinos.Count; i++)
        {
            if(dinos[i].name.Equals(d.name)){
                dinos[i]=d;
                return;
            }
        }
    }
    public void SetFoodbyId(Food f){
        for (int i = 0; i < foodList.Count; i++)
        {
            if(foodList[i].id == f.id){
                foodList[i]=f;
                return;
            }
        }
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
