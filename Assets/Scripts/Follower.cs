﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform Target;
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Target != null)
        {
            //print(Target.position.x+" x   "+Target.position.y+" y      "+Target.position.z+" z");
           // transform.position =transform.TransformPoint(Target.position);
            transform.position = new Vector3(Target.position.x,Target.position.y,transform.position.z);
        }
    }
}
