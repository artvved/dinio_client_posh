﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    public GameObject FoodPrefab;
    public Material MeatMaterial;
    public Material GrassMaterial;



    public GameObject SpawnFood(Food food)
    {
        var foodGo = Instantiate(FoodPrefab);
        foodGo.transform.position = new Vector3(food.myTransform.x,
                                   food.myTransform.y, foodGo.transform.position.z);//local pos earlier

        foodGo.transform.eulerAngles = new Vector3(foodGo.transform.eulerAngles.x,//trans.Angle earlier
                                foodGo.transform.eulerAngles.y, food.myTransform.rotation);
        foodGo.transform.localScale = new Vector3(food.myTransform.scaleX,
                                food.myTransform.scaleY, foodGo.transform.localScale.z);
        if (food.GetType().Equals(typeof(Grass)))
            foodGo.GetComponent<MeshRenderer>().material = GrassMaterial;
        else
            foodGo.GetComponent<MeshRenderer>().material = MeatMaterial;
        return foodGo;

    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
