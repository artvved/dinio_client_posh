﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class RoomMenuScript : MonoBehaviour
{
    public InputField NameField;
    public InputField CountField;
    public Toggle toggle;
    public GameObject ExceptionWindow;
    public Text eText;
    public InputField InputField;
    //public Buttion OkButton;
    //public Buttion BackButton;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public async void FastGame()
    {
        try
        {
            await RoomConnection.RandomRequestAsync();
            SceneManager.LoadScene("GameScene");
        }
        catch (Exception e)
        {
            ExceptionWindow.SetActive(true);
            eText.text = e.Message;
        }
    }

    public async void FindRoom()
    {
        Debug.Log("StartFind");
        string name = InputField.text;
        if (name.Length != 0)
        {
            try
            {
                await RoomConnection.GetRequestAsync(name);
                SceneManager.LoadScene("GameScene");
            }
            catch (Exception e)
            {
                ExceptionWindow.SetActive(true);
                eText.text = e.Message;
            }

        }
        else
        {
            ExceptionWindow.SetActive(true);
            eText.text = "Введите название";
        }
    }

    public async void CreateRoom()
    {
        Debug.Log("Start");
        string name = NameField.text;
        string count = CountField.text;
        bool isPrivate = toggle.isOn;
        if (name.Length != 0 && count.Length != 0)
        {

            int intCount = Int32.Parse(count);
            if (intCount > 0 && intCount < 20)
            {
                try
                {
                    await RoomConnection.CreateRequestAsync(name, intCount, isPrivate);
                    Debug.Log("Ok");
                    SceneManager.LoadSceneAsync("GameScene");
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                    ExceptionWindow.SetActive(true);
                    eText.text = e.Message;
                }
            }
            else
            {
                Debug.Log("Wrong max count");
                ExceptionWindow.SetActive(true);
                eText.text = "Макимальное количество людей в комнате - от одного до двадцати";
            }
        }
        else
        {
            Debug.Log("Fields are empty");
            ExceptionWindow.SetActive(true);
            eText.text = "Введите значения";
        }
    }

    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
