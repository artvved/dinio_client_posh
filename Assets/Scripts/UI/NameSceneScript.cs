﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using dinio_client;
using Microsoft.Win32;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement;

public class NameSceneScript : MonoBehaviour
{
    public InputField inputField;
    public Button button;
    public GameObject ExceptionWindow;
    public Text eText;
    public GameObject NameEnter;
    public GameObject Download;

    async void Start()
    {
        User.device_id = SystemInfo.deviceUniqueIdentifier;
        await NameConnection.AuthRequestAsync(User.device_id);
        if (User.name != null && User.name.Length != 0 && !NameCh.changeName)
        {
            Debug.Log(User.name);
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            Download.SetActive(false);
            NameEnter.SetActive(true);
        }
        button.onClick.AddListener(OnButtonClicked);
    }

    public async void OnButtonClicked()
    {
        string text = inputField.text;
        if (text.Length != 0)
        {
            User.name = text;
            try
            {
                if (NameCh.changeName)
                {

                    await NameConnection.ChangeNameRequestAsync();
                    NameCh.changeName = false;
                    SceneManager.LoadScene("MainMenu");
                }
                else
                {
                    await NameConnection.RegisterRequestAsync();
                    SceneManager.LoadScene("MainMenu");
                }
            } catch(Exception e)
            {
                ExceptionWindow.SetActive(true);
                eText.text = e.Message;
            }
            
            
        }
    }

}
