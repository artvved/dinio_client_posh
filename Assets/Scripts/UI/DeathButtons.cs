﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathButtons : MonoBehaviour
{
   
    public GameObject GameUI;

    private void OnEnable() {
        GameUI.SetActive(false);
    }
}
