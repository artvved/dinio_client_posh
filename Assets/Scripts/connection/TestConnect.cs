﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq.Expressions;
using System.Net;
using WebSocketSharp;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace dinio_client
{
    public class TestConnect
    {

        private WebSocket ws;
        private StompMessageSerializer serializer;

        public void Start()//передть параметром куда подключаться
        {
            Debug.Log("Start");
            ws = new WebSocket("ws://localhost:8080/gs-guide-websocket/websocket");


            int clientId = 999;
            serializer = new StompMessageSerializer();

            ws.OnOpen += (sender, e) =>
            {
                Debug.Log("on open");

                var connect = new StompMessage("CONNECT");
                connect["accept-version"] = "1.1";
                connect["heart-beat"] = "10000,10000";
                ws.Send(serializer.Serialize(connect));

                var sub = new StompMessage("SUBSCRIBE");
                sub["id"] = "sub-" + clientId;
                sub["destination"] = "/topic/"+ Room.name;
                ws.Send(serializer.Serialize(sub));

            };

            ws.OnClose += (sender, e) =>
             {
                 Debug.Log("CLose: " + e.Reason);

             };

            ws.OnError += (sender, e) =>
            {
                Debug.Log("Error: " + e.Message);

            };
            ws.OnMessage += (sender, e) =>
            {

                //Debug.Log("Spring says: ");
                string s = parse(e.Data);
                //Debug.Log( MsgQueue.instance.Count());
                MsgQueue.instance.push(s);
                




            };
            ws.Connect();

            //Send("msg from client");


            //Console.ReadKey(true);

        }


        public void Send(string s)
        {
            var send = new StompMessage("SEND");
            Message msg = new Message(s);
            string json = JsonConvert.SerializeObject(msg);
            send["destination"] = "/app/" + Room.name; //задать полем инициализировать при старте конекта
            send["content-length"] = json.Length.ToString();
            send.Body = json;

            ws.Send(serializer.Serialize(send));

        }


        public string parse(string s)
        {
            char[] chs = s.ToCharArray();
            int count = 0;
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var ch in chs)
            {
                if (ch == '{')
                {
                    count++;
                }

                if (count == 0)
                {
                    continue;
                }
                else
                {
                    stringBuilder.Append(ch);
                }

                if (ch == '}')
                {
                    count--;
                }

            }
            return stringBuilder.ToString();
        }

    }




    class Message
    {
        public string name { get; set; }

        public Message(string Name)
        {
            name = Name;
        }
    }
}

