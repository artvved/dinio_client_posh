using System.Collections;
using System.Collections.Generic;
using dinio_client;
using UnityEngine;

public class MonoConnection : MonoBehaviour
{
    public TestConnect TestConnect{get;set;}
    
    void Start()
    {
        TestConnect = new TestConnect();
        TestConnect.Start();
    }

}
