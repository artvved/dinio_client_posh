﻿using Microsoft.Win32;
using System.Collections;
using System.Collections.Generic;
using dinio_client;
using UnityEngine;
using System;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System.Threading.Tasks;

public class RoomConnection : MonoBehaviour
{
    public static async Task CreateRequestAsync(string name, int maxCountOfPlayers, bool isPrivate)
    {

        Debug.Log("start");
        WebRequest request = WebRequest.Create("http://localhost:8080/room/create");
        StompMessageSerializer serializer = new StompMessageSerializer();
        request.Method = "POST";
        RoomDto roomDto = new RoomDto(name, maxCountOfPlayers, 0, isPrivate);
        string data = JsonConvert.SerializeObject(roomDto);
        Debug.Log(data);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }

        try
        {

            WebResponse response = await request.GetResponseAsync();

            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String s = reader.ReadToEnd();
                    Room.name = name;
                    Room.maxCountOfPlayers = maxCountOfPlayers;
                    Room.currentCountOfPlayers = 0;
                    Room.isPrivate = isPrivate;
                }
            }
            response.Close();
        }

        catch (WebException e)
        {

            Debug.Log(e.Message);
            throw new Exception("Комната с таким названием уже существует");

        }
    }

    public static async Task RandomRequestAsync()
    {
        WebRequest request = WebRequest.Create("http://localhost:8080/room/random");
        request.Credentials = CredentialCache.DefaultCredentials;

        try
        {
            WebResponse response = request.GetResponse();
            RoomDto roomDto = new RoomDto();
            using (Stream dataStream = response.GetResponseStream())
            {
                
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    String s = reader.ReadToEnd();
                    roomDto = JsonConvert.DeserializeObject<RoomDto>(s);
                    Room.name = roomDto.name;
                    Room.maxCountOfPlayers = roomDto.maxCountOfPlayers;
                    Room.currentCountOfPlayers = roomDto.currentCountOfPlayers;
                    Room.isPrivate = roomDto.isPrivate;
                }
            }
            response.Close();
        }

        catch (WebException e)
        {

            Debug.Log(e.Message);
            throw new Exception("Комнат нет");

        }
    }

    public static async Task GetRequestAsync(string name)
    {
        Debug.Log("start");
        WebRequest request = WebRequest.Create("http://localhost:8080/room/get");
        StompMessageSerializer serializer = new StompMessageSerializer();
        request.Method = "POST";
        RoomName roomName = new RoomName(name);
        
        Debug.Log(name);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(name);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }

        RoomDto roomDto = new RoomDto();
        try
        {
            WebResponse response = await request.GetResponseAsync();
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String s = reader.ReadToEnd();
                    roomDto = JsonConvert.DeserializeObject<RoomDto>(s);
                    Room.name = roomDto.name;
                    Room.maxCountOfPlayers = roomDto.maxCountOfPlayers;
                    Room.currentCountOfPlayers = roomDto.currentCountOfPlayers;
                    Room.isPrivate = roomDto.isPrivate;
                }
            }
            response.Close();
        }
        catch (WebException e)
        {
            Debug.Log(e.Message);
            throw (new Exception("Комната с таким названием не существует"));
        }
    }

    public static async Task DelRequestAsync(string name)
    {
        Debug.Log("start");
        WebRequest request = WebRequest.Create("http://localhost:8080/room/del");
        StompMessageSerializer serializer = new StompMessageSerializer();
        request.Method = "POST";
        RoomName roomName = new RoomName(name);
        string data = JsonConvert.SerializeObject(roomName);
        Debug.Log(data);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }
    }

}

[Serializable]
class RoomDto
{
    public string name { get; set; }
    public int maxCountOfPlayers { get; set; }
    public int currentCountOfPlayers { get; set; }
    public bool isPrivate { get; set; }

    public RoomDto(string Name, int MaxCountOfPlayers, int CurrentCountOfPlayers, bool IsPrivate)
    {
        name = Name;
        maxCountOfPlayers = MaxCountOfPlayers;
        currentCountOfPlayers = CurrentCountOfPlayers;
        isPrivate = IsPrivate;
    }

    public RoomDto()
    {

    }
}

[Serializable]
class RoomName
{
    public string name { get; set; }

    public RoomName(string Name)
    {
        name = Name;
    }
}