﻿using Microsoft.Win32;
using System.Collections;
using System.Collections.Generic;
using dinio_client;
using UnityEngine;
using System;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System.Threading.Tasks;


public class NameConnection : MonoBehaviour
{
    public static async Task RegisterRequestAsync()
    {

        //Debug.Log("Start");
        WebRequest request = WebRequest.Create("http://localhost:8080/auth/reg");
        StompMessageSerializer serializer = new StompMessageSerializer();
        request.Method = "POST";
        UserDto userDto = new UserDto(User.name, User.device_id);
        string data = JsonConvert.SerializeObject(userDto);
        //Debug.Log(data);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }

        try
        {
            WebResponse response = await request.GetResponseAsync();
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String s = reader.ReadToEnd();
                    AuthRequestAsync(User.device_id);
                }
            }
            response.Close();
        }
        catch (WebException e)
        {
            Debug.Log(e.Message);
            throw (new Exception("Пользователь с таким именем уже существует"));
        }
    }

    public static async Task AuthRequestAsync(String device_id)
    {
        //Debug.Log("Start");
        WebRequest request = WebRequest.Create("http://localhost:8080/auth/");
        StompMessageSerializer serializer = new StompMessageSerializer();

        request.Method = "POST";
        //Debug.Log(data);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(device_id);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }
        UserDto userdto;
        
        try
        {
            WebResponse response = await request.GetResponseAsync();
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String s = reader.ReadToEnd();
                    userdto = JsonConvert.DeserializeObject<UserDto>(s);
                    User.device_id = userdto.device_id;
                    User.name = userdto.name;

                }
            }
            response.Close();
        }
        catch (WebException e)
        {
            Debug.Log(e.Message);
            throw (new Exception("Проблемы с авторизацией"));
        }
        

    }

    public static async Task ChangeNameRequestAsync()
    {

        Debug.Log("start");
        WebRequest request = WebRequest.Create("http://localhost:8080/auth/ch");
        StompMessageSerializer serializer = new StompMessageSerializer();
        request.Method = "POST";
        UserDto userDto = new UserDto(User.name, User.device_id);
        string data = JsonConvert.SerializeObject(userDto);
        Debug.Log(data);
        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
        request.ContentType = "application/json";
        request.ContentLength = byteArray.Length;
        using (Stream dataStream = request.GetRequestStream())
        {
            dataStream.Write(byteArray, 0, byteArray.Length);
        }

        try
        {

            WebResponse response = await request.GetResponseAsync();

            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    String s = reader.ReadToEnd();
                }
            }
            response.Close();
        }

        catch (WebException e)
        {

            Debug.Log(e.Message);
            throw new Exception("Игрок с таким именем уже существует");

        }
    }

    
}

[Serializable]
class UserDto
{
    public string name { get; set; }
    public string device_id { get; set; }

    public UserDto(string Name, string Device_id)
    {
        name = Name;
        device_id = Device_id;
    }
}
