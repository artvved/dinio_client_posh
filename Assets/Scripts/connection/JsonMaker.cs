﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

/**
* class to work with json
*/
public class JsonMaker
{

    public static string ToJson(object o)
    {


        string nJson = JsonConvert.SerializeObject(o, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects

        });
        nJson = nJson.Replace("$", string.Empty);
        nJson= Room.name+nJson;
        if(GameStateChangeController.instance.GetChanges()){
            nJson="1"+nJson;
        }else{
             nJson="0"+nJson;
        }

        return nJson;
    }

    public static object FromJson(string json)
    {
        json = json.Replace("\"type\"", "\"$type\"");

        object o = JsonConvert.DeserializeObject(json, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects

        });
        return o;
    }
}
