﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgQueue
{


    public static MsgQueue instance = new MsgQueue();

    private Queue<string> queue = new Queue<string>();
    private MsgQueue() { }

    public void push(string s)
    {
        lock (queue)
        {
            if(!isEmpty()){
               // queue.Dequeue(); //making queue contain 1 el //questionable
            }
            queue.Enqueue(s);
        }
    }

    public string pop()
    {
        lock (queue)
        {
            string s = queue.Dequeue();
            //Debug.Log(s);
            return s;
        }
    }

    public bool isEmpty()
    {
        //Debug.Log(queue.Count);
        lock (queue)
        {
            return queue.Count == 0;
        }
    }

    public int Count(){
        lock (queue)
        {
            return queue.Count;
        }
    }



}
